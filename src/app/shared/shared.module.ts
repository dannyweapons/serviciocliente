import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';
import { FilterPipe } from '../pipes/filterdata.pipe';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from '../pages/pages.component';

const routes: Routes = [
    {
      path: '',
      component: PagesComponent
    },
  ];
  

@NgModule({
    declarations: [
        NopagefoundComponent,
        HeaderComponent,
        SidebarComponent,
        FilterPipe
    ],
    exports: [
        NopagefoundComponent,
        HeaderComponent,
        SidebarComponent,
        FilterPipe
    ],
    imports: [
        RouterModule.forChild(routes)
    ],
    providers: []
})
export class SharedModule {
}
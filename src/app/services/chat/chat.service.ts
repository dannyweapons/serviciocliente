import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import * as TwilioChat from 'twilio-chat';

export class ChatMessage {
  userId: string;
  time: number | string;
  message: string;
  type: string;
  url: any;
  attributes: any;
  contentType: any;
  filename: any;
}

@Injectable({
  providedIn: 'root'
})

export class ChatService {

  loading: boolean = false;  // variable que indica si está cargando el token o mensajes de twilio
  private loadingMessagesSource = new BehaviorSubject<any>(this.loading);
  // tslint:disable-next-line: max-line-length
  loadingMessagesActualizado = this.loadingMessagesSource.asObservable(); // Observable utilizado en el controlador de chat para mostrar el spinner de carga.

  public msgList: ChatMessage[] = []; // lista de mensajes en chat
  public msgListSC: any = [];

  private msgListSource = new BehaviorSubject<any>(this.msgList);
  msgListActualizada = this.msgListSource.asObservable(); // Observable utilizado en el controlador de chat para mostrar mensajes.

  MESSAGES_HISTORY_LIMIT = 50; // Mostrar máximo 50 mensajes de cada conversación
  public accessManager: any;
  public tc: any;

  constructor(private http: HttpClient) {
    this.tc = { // objecto que se asocia a un canal de twilio
      messagingClient: null,
      currentChannel: null,
      username: null
    };
  }

  ngOnDestroy() {
    this.loadingMessagesSource.unsubscribe(); // importante unsubscribe para no acumular observables
    this.msgListSource.unsubscribe();
  }

  initializeTwilioChatClient(idUsuario: any) {  // Pedir token a prod para después iniciar cliente de twilio
    return new Promise((resolve, reject) => {
      let self = this;
      this.msgListSC = [];
      const body = new URLSearchParams();
      body.set('device', 'mobile');
      body.set('identity', idUsuario);

      this.http.post('https://08f8cf58.ngrok.io/customer_service_chat/token/', body.toString())
        .subscribe(data => {
          self.loadingMessagesSource.next(true);
          const token = data['token'];
          TwilioChat.Client.create(token).then((client) => {
            self.tc.messagingClient = client;
            self.tc.messagingClient.initialize().then(() => {
              self.tc.messagingClient.on('tokenExpired', () => { self.refreshToken() });
              return resolve();
            }).catch((error) => {
              self.loadingMessagesSource.next(false);
              return reject(error);
            });
          }).catch((error) => {
            self.loadingMessagesSource.next(false);
            return reject(error);
          });
        }, err => {
          self.loadingMessagesSource.next(false);
          return reject(err);
        });
    });
  }

  connectToChatChannel(idUsuario) { // Conectarse a un canal o conversación de twilio. Cargar mensajes.
    let self = this;

    self.tc.messagingClient.getChannelByUniqueName(idUsuario).then((channel) => {
        //this.updateMsgList([]);
        self.loadingMessagesSource.next(true);
        this.leaveCurrentChannel().then(() => {
          this.joinChannel(channel).then(() => {
            this.tc.currentChannel.removeAllListeners();
            self.tc.currentChannel.on('messageAdded', (message) => {
              const mensaje = { 'mensaje': message.state.body, 'autor': message.author }
              this.msgListSC.push(mensaje);
            });
          }).catch((error) => {


            console.log(('joinChannel' + JSON.stringify(error, Object.getOwnPropertyNames(error))));
          });
        }).catch((error) => {
        });
    }).catch((error) => {
      console.log(('getChannelByUniqueName' + JSON.stringify(error, Object.getOwnPropertyNames(error))));
    });
  }

  fetchAccessToken(username, handler) {
    return new Promise((resolve, reject) => {
      this.http.post('https://08f8cf58.ngrok.io/customer_service_chat/token', { device: 'mobile', identity: username })
        .subscribe(data => {
          var token = data['token'];
          handler(token);
          return resolve();
        }, err => {
          // console.log(JSON.stringify(err));
          return reject(err);
        });
    });
  }

  updateMsgList(data: any) { // actualizar observable cuando se cambia de conversación o cuando llega un mensaje nuevo
    console.log('linea 116', data);
    this.msgListSource.next(data);
  }

  refreshToken() {
    const self = this;
    self.fetchAccessToken(self.tc.username, self.setNewToken.bind(self));
  }

  setNewToken(token) {
    const self = this;
    self.accessManager.updateToken(token);
    window.location.reload();
  }

  joinChannel(_channel) {
    const self = this;
    return _channel.join().then((joinedChannel) => {
      self.tc.currentChannel = _channel;
      _channel.getMessages().then((messages) => {
        const totalMessages = messages.items.length;
        for (let i = 0; i < totalMessages; i++) {
          const message = messages.items[i].state.body;
          const usuario = messages.items[i].state.author;
          const mensaje = { 'mensaje': message, 'autor': usuario }
          self.agregandoMensaje(mensaje);
        }
      });
      return joinedChannel;
    }).catch(function (error) {
      self.tc.currentChannel = _channel;
      _channel.getMessages().then((messages) => {
        const totalMessages = messages.items.length;
        for (let i = 0; i < totalMessages; i++) {
          const message = messages.items[i].state.body;
          const usuario = messages.items[i].state.author;
          const mensaje = { 'mensaje': message, 'autor': usuario }
          self.agregandoMensaje(mensaje);
          // self.loadMessages('setupchannel');
        }
      });
      return _channel;
    });
  }

  agregandoMensaje(mensaje) {
    this.msgListSC.push(mensaje);
  }

  leaveCurrentChannel() {
    let self = this;

    if (self.tc.currentChannel) {
      return self.tc.currentChannel.leave().then((leftChannel) => {
        // this.longitudConversacionSource.next(0);
        leftChannel.removeListener('messageAdded', () => { console.log('leaving current channel') });

        return Promise.resolve();
      }).catch(function (error) {
        // alert('hey boy');
      });
    } else {
      // alert('no hay ningún canal');
      return Promise.resolve();
    }
  }
}





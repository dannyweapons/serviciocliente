import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './chat/chat.component';
import { GuarantyComponent } from './guaranty/guaranty.component';
import { UpgradeComponent } from './upgrade/upgrade.component';

const routes: Routes = [
    {
        path: 'chat',
        component: ChatComponent
    },
    {
        path: 'guaranty',
        component: GuarantyComponent
    },
    {
        path: 'upgrade',
        component: UpgradeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PagesRoutingModule { }
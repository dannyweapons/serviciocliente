import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { GuarantyComponent } from './guaranty/guaranty.component';
import { ChatComponent } from './chat/chat.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import { PagesComponent } from './pages.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    GuarantyComponent,
    ChatComponent,
    UpgradeComponent,
    PagesComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule,
    FormsModule,
    NgxSpinnerModule
  ]
})
export class PagesModule {}
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from 'src/app/services/chat/chat.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Subscription } from 'rxjs/index';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})

export class ChatComponent implements OnInit, OnDestroy {

  public clientes;
  public busqueda;
  public clienteActual;
  public mensaje;
  selectedIndex = 0;
  public msgListSC: any = [];
  private msgListSource = new BehaviorSubject<any>(this.msgListSC);
  msgListActualizada = this.msgListSource.asObservable();
  public selectedLeadSuscription: Subscription;

  constructor(
    public chatService: ChatService,
    public http: HttpClient,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.getChats();
  }

  ngOnDestroy() {
    this.selectedLeadSuscription.unsubscribe();
  }

  setIndex(index: number) {
    this.selectedIndex = index;
  }

  public getChats() {

    const body = new URLSearchParams();
    body.set('token', 'K00mk1n@!xWz93OTkwMSwiZX');

    const options = {
      headers: new HttpHeaders().set(
        'Content-Type',
        'application/x-www-form-urlencoded'
      )
    };

    return new Promise((resolve, reject) => {
      this.http.post('https://08f8cf58.ngrok.io/customer_service_chat/cs_chat_insights/', body.toString(), options).subscribe(
        data => {
          console.log(data);
          this.clientes = data;
        },
        err => {
          return reject(err);
        }
      );
    });
  }

  public getMessages(idUsuario) {
    this.spinner.show();
    this.chatService.initializeTwilioChatClient(idUsuario).then(() => {

      this.chatService.connectToChatChannel(idUsuario);
      this.selectedLeadSuscription = this.chatService.msgListActualizada.subscribe(
        result => {
          this.spinner.hide();
          if (result.length === 0) {
            this.msgListSC = result;
          }
          if (result.length > 1) {
            this.msgListSC = result;
          } else {
            var temp = result[0];
            this.msgListSC = this.msgListSC.concat(result);
          }
        }
      );
      this.msgListSC = this.chatService.msgListSC;
    }).catch(()=>{
      console.log('twilio token fetched error');
    });
  }

  public verCliente(cliente) {
    this.getMessages(cliente.idusuario.toString());
    this.clienteActual = cliente;
  }

  public sendMessage(cliente) {

    const mas = '+';
    const telefono = mas.concat(cliente.userphone);

    if (this.mensaje !== undefined || this.mensaje !== '') {
      const body = new URLSearchParams();
      body.set('Body', this.mensaje);
      body.set('To', telefono);
      body.set('From', '+525585268341');

      const options = {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      };

      return new Promise((resolve, reject) => {
        this.http.post('https://08f8cf58.ngrok.io/customer_service_chat/msg_handler/', body.toString(), options).subscribe(
          data => {
            if (data == null) {
              this.mensaje = '';
            }
          },
          err => {
            console.log(err);
            return reject(err);
          }
        );
      });
    }
  }
}
